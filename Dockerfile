FROM fluent/fluentd
RUN echo "http://mirrors.aliyun.com/alpine/v3.5/main/" > /etc/apk/repositories && \   
    echo "http://mirrors.aliyun.com/alpine/v3.5/community/" >> /etc/apk/repositories && \    
    echo "http://mirrors.aliyun.com/alpine/v3.5/releases/" >> /etc/apk/repositories && \ 
    apk add --no-cache --virtual .build-deps sudo build-base ruby-dev && \
    sudo gem sources --add https://gems.ruby-china.org/ --remove https://rubygems.org/ && \
    sudo gem install fluent-plugin-elasticsearch --no-document && \
    sudo gem install fluent-plugin-mongo --no-document && \
    sudo gem install fluent-plugin-record-modifier --no-document && \
    sudo gem install fluent-plugin-rewrite-tag-filter --no-document && \
    sudo gem install fluent-plugin-rewrite --no-document && \
    sudo gem install fluent-plugin-concat --no-document && \
    sudo gem sources --clear-all && \
    apk del .build-deps && \
    apk add --no-cache tzdata && \
    rm -rf /home/fluent/.gem/ruby/2.3.0/cache/*.gem
ENV TZ Asia/Shanghai
