# 安装
```
git clone https://gitee.com/gateray/logcentralized.git
docker stack deploy -c docker-stack-compose.yml --with-registry-auth logcentralized
```
安装完毕后，默认会在本地启动2个elasticsearch、1个kibana、1个fluentd实例，如下：
```
➜  ~ docker service ls
ID                  NAME                            MODE                REPLICAS            IMAGE                                                 PORTS
mkhrhabwvj9d        logcentralized_elasticsearch1   replicated          1/1                 docker.elastic.co/elasticsearch/elasticsearch:6.1.2   *:9200->9200/tcp
zyby8ai71kgj        logcentralized_elasticsearch2   replicated          1/1                 docker.elastic.co/elasticsearch/elasticsearch:6.1.2   
0ld5le5ptz41        logcentralized_fluentd          replicated          1/1                 private-fluentd:v1.3                                  *:24224->24224/tcp,*:24224->24224/udp
h0z6kevfg934        logcentralized_kibana           replicated          1/1                 docker.elastic.co/kibana/kibana:6.1.2                 *:5601->5601/tcp
```
> 如果es无法启动，请检查如下系统设置：
```
1. 内核参数
vim /etc/sysctl.conf
vm.max_map_count = 262144
2. memlock限制
vim /lib/systemd/system/docker.service
LimitMEMLOCK=infinity

systemctl daemon-reload
systemctl restart docker.servicesyste
```

# 使用
## 日志服务端（fluentd+elasticsearch+kibana）
### 组件介绍
- fluentd： 用于接收docker容器输出的日志流、解析、处理，最终写入elasticsearch；同时也可用作日志分析、监控、告警等处理。
- elasticsearch：存储、索引fluentd输入的日志，可用于后期的分析、统计。
- kibana：提供了日志分析的可视化UI
启动完毕后，本地监听24224/tcp和24224/udp（接收客户端的日志输入请求）、9200/tcp端口（elaticsearch访问端口）、5601/tcp（kibana访问端口）

## 日志客户端
从docker1.8版本开始，提供了fluentd日志驱动，通过forward的方式可以将容器的标准输出、标准错误输出通过fluent协议转发到fluentd server
### 命令行启动容器
```
docker run -it --name testnginx -p 80:80 --log-driver fluentd --log-opt tag=nginx private-nginx:alpine
```
> --log-driver: 用于指定日志驱动
>
> --log-opt: 指定日志驱动的选项，tag属性可以对fluentd接收的日志事件进行分类, 如果fluentd server与客户端不在同一台服务器或者不是监听在默认端口，可以通过fluentd-address=fluentdhost:24224来指定fluentd server的ip和监听端口

### docker compose方式
```
--- file: docker-compose.yml
version: "3"
services:
  nginx:
    image: private-nginx:alpine
    logging:
      driver: "fluentd"
      options:
        tag: "nginx"
        fluentd-address: "localhost:24224"
    ports:
      - target: 80
        published: 80
```

> 为了让fluentd方便处理nginx access log，nginx中accesslog使用了json格式输出：
```
log_format jsondoc '{' 
                    '"@timestamp":"$time_iso8601",'
                    '"hostname":"$hostname",'
                    '"domain":"$host",'
                    '"proxyip":"$remote_addr",'
                    '"clientip":"$http_x_forwarded_for",'
                    '"status":$status,'
                    '"size":$body_bytes_sent,'
                    '"request_time":$request_time,'
                    '"user_agent":"$http_user_agent",'
                    '"method":"$request_method",'
                    '"referer":"$http_referer",'
                    '"schema":"$scheme",'
                    '"uri":"$uri",'
                    '"query_string":"$query_string",'
                    '"request_body":"$request_body"'
                    '}';
access_log /dev/stdout jsondoc;
```
# 参考文档
- fluentd日志驱动 https://docs.docker.com/config/containers/logging/fluentd/
- fluentd官方文档 https://docs.fluentd.org/v1.0/articles/quickstart
- fluentd所有插件介绍 https://www.fluentd.org/plugins/all#input-output
- fluentd日志监控使用场景介绍 https://docs.fluentd.org/v1.0/articles/free-alternative-to-splunk-by-fluentd
- docker输出多行日志（如异常堆栈）的处理方式 https://www.fluentd.org/guides/recipes/docker-logging